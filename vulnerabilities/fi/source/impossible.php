<?php

// Use this pattern to allow calling every file in the source-folder if the name contains only numbers and characters:
#$pattern = '/^[a-zA-Z0-9]+[.]php$/';

// Use this pattern to allow calling file1.php, file2.php, file3.php and include.php only:
$pattern = '/^(file[1-3]|include)+[.]php$/';

if (preg_match($pattern, $_GET['page'])) {
    $file = $_GET['page'];
} else {
    $file = 'include.php';
    $html = 'This file is not allowed!';
}

?>
