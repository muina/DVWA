<?php

if( isset( $_POST[ 'Submit' ]  ) ) {
	// Check Anti-CSRF token
	checkToken( $_REQUEST[ 'user_token' ], $_SESSION[ 'session_token' ], 'index.php' );
	
	// Get input
	$target = trim($_REQUEST[ 'ip' ]);
	$target = stripslashes($target);
	
	// check if input is ipv4 or ipv6 adress
	if (filter_var($target, FILTER_VALIDATE_IP)) { 
		// Determine OS and execute the ping command.
		if( stristr( php_uname( 's' ), 'Windows NT' ) ) {
			// Windows
			$cmd = shell_exec( 'ping  ' . $target );
		}
		else {
			// *nix
			$cmd = shell_exec( 'ping  -c 4 ' . $target );
		}

		// Feedback for the end user
		$html .= "<pre>{$cmd}</pre>";
	}
	else {
		// error if input is wrong
		$html .= '<pre>Error: Wrong IP-adress</pre>';
	}
}

// Generate Anti-CSRF token
generateSessionToken();

?>
