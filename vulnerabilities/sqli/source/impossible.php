<?php

if (isset($_REQUEST['Submit'])) {
    // Get input
    $id = $_REQUEST['id'];

    // Validate if input is a number
    $pattern = '/^[0-9]+$/';

    if (preg_match($pattern, $id) == 1) {
        switch ($_DVWA['SQLI_DB']) {
            case MYSQL:
                // Check database
                $query = $db->prepare('SELECT first_name, last_name FROM users WHERE user_id = (:id) LIMIT 1;');
                $query->bindParam(':id', $id, PDO::PARAM_INT);
                $query->execute();
                $row = $query->fetch();

                // Get values
                $first = $row["first_name"];
                $last = $row["last_name"];

                // Feedback for end user
                $html .= "<pre>ID: {$id}<br />First name: {$first}<br />Surname: {$last}</pre>";

                mysqli_close($GLOBALS["___mysqli_ston"]);
                break;
            case SQLITE:
                $html .= "We only implemented solutions for MySQL / MariaDB. Please consider using one of these databases.";
        }
    } else {
        $html .= "<pre>Only (positive) numbers are allowed!</pre>";
    }
}

?>
