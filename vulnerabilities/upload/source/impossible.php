<?php

if (isset($_POST['Upload'])) {
    // Create salted and hashed filename and check, if it already exists
    $extension = end(explode(".", $_FILES['uploaded']['name']));
    while (True) {
        $hashvalue = ($_FILES['uploaded']['name']) . time();
        $hashname = hash(sha256, $hashvalue, false) . "." . $extension;

        // Where are we going to be writing to?
        $target_path = DVWA_WEB_PAGE_TO_ROOT . "../uploads/";
        $target_path .= basename($hashname);
        if (!file_exists($target_path)) {
            break;
        }
        sleep(1);
    }

    // Checking File Size
    $filesize = $_FILES['uploaded']['size'];
    if (1000 < $filesize && $filesize < 52428800) {
        // Checking File-Extension
        $extension_allowed = array("gif", "jpg", "jpeg", "ico", "png", "tiff", "tif", "bmp", "svg", "webp");
        if (in_array($extension, $extension_allowed)) {

            // Checking Magic Numbers
            $mime = mime_content_type($_FILES['uploaded']['tmp_name']);
            $mime_allowed = array("image/bmp", "image/gif", "image/x-icon", "image/jpeg", "image/png", "image/tiff", "image/webp", "image/vnd.microsoft.icon");

            if (in_array($mime, $mime_allowed)) {

                // Can we move the file to the upload folder?
                if (!move_uploaded_file($_FILES['uploaded']['tmp_name'], $target_path)) {
                    // No
                    $html .= '<pre>Your image was not uploaded.</pre>';
                } else {
                    // Yes!
                    $html .= "<pre>Your file was uploaded successfully!</pre>";
                }
            } else {
                $html .= "<pre>Wrong datatype! File was not uploaded! {$mime}</pre>";
            }

        } else {
            $html .= "<pre>Wrong datatype! File was not uploaded! {$extension}</pre>";
        }


    } else {
        $html .= "<pre>Invalid file size (max. 50 MB)! File was not uploaded! {$extension}</pre>";
    }
}

?>
