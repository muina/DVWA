<?php

if( isset( $_POST[ 'btnSign' ] ) ) {
	// Check Anti-CSRF token
	checkToken( $_REQUEST[ 'user_token' ], $_SESSION[ 'session_token' ], 'index.php' );

	// Get input
	$message = trim( $_POST[ 'mtxMessage' ] );
	$name    = trim( $_POST[ 'txtName' ] );

	// Sanitize message input
	$message = stripslashes( $message ); // Took from low.php
	$message = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"],  $message ) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : "")); // Took from low.php
	$message = htmlspecialchars( $message ); // Encode chars like <, > and & that browser doesn't execute it as HTML

	// Sanitize name input, same as with message
	$name = stripslashes( $name );
	$name = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"],  $name ) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
	$name = htmlspecialchars( $name );

	// Update database with prepared statement to prevent SQL injection Documentation: https://www.php.net/manual/en/pdo.prepared-statements.php
	$statement = $db->prepare("INSERT INTO guestbook (comment, name) VALUES (:message, :name)");
	$statement->bindParam(":message", $message);
	$statement->bindParam(":name", $name);
	$statement->execute();
}

// Generate Anti-CSRF token
generateSessionToken();

?>
