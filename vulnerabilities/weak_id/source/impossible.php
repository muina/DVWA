<?php

$html = "";
$time = time();

# Logout if button is pressed or the time limit is over:
if (isset($_POST['logout']) || (isset($_SESSION['creation_time']) && ($time - $_SESSION['creation_time'] > 3600))) {
    unset($_SESSION['last_session_id']);
    unset($_SESSION['creation_time']);
    setcookie("dvwaSession", "", time() + 1);
} else if (isset($_POST['generate'])) {

    # Generating Session-ID using 256 bit
    $session_id = bin2hex(random_bytes(32));

    # Set new Session-ID serverside:
    $_SESSION['last_session_id'] = $session_id;
    $_SESSION['creation_time'] = $time;

    # Set net Session-ID clientside
    $cookie_value = $_SESSION['last_session_id'];
    setcookie("dvwaSession", $cookie_value, time() + 3600);
}


?> 
