<?php
// Is there any input?
if( array_key_exists( "name", $_GET ) && $_GET[ 'name' ] != NULL ) {
	// Check Anti-CSRF token
	checkToken( $_REQUEST[ 'user_token' ], $_SESSION[ 'session_token' ], 'index.php' );

	// Feedback for end user
	$html .= '<pre>Hello ' . htmlspecialchars($_GET[ 'name' ]) . '</pre>'; // Use htmlspecialchars() for input string to encode <, > and & that browsers won't interpret it as HTML
}

// Generate Anti-CSRF token
generateSessionToken();

?>
