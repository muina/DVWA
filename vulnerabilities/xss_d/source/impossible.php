<?php

// Is there any input?
if ( array_key_exists( "default", $_GET ) && !is_null ($_GET[ 'default' ]) ) {
    $default = $_GET['default'];

    # Only allow specific input and in case of not-allowed input fall back to English
    if ($default != "English" && $default != "French" && $default != "Spanish" && $default != "Spanish" && $default != "German") {
        header ("location: ?default=English");
        exit;
    }
}

?>
